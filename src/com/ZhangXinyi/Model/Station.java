package com.ZhangXinyi.Model;

public class Station {

    /**
     * stationName 站点名称
     * isTerminal 是否为终点站
     * distanceToPreStation 距离前站距离
     * distanceToNextStation 距离下站距离
     */
    private String stationName;
    private boolean isTerminal;
    private float distanceToPreStation;
    private float distanceToNextStation;

    public Station(String stationName,boolean isTerminal,float distanceToNextStation,float distanceToPreStation){
        this.stationName=stationName;
        this.distanceToNextStation=distanceToNextStation;
        this.isTerminal=isTerminal;
        this.distanceToPreStation=distanceToPreStation;
    }

    public Station(String stationName,boolean isTerminal,float distanceToNextStation){
        this.stationName=stationName;
        this.distanceToNextStation=distanceToNextStation;
        this.isTerminal=isTerminal;
    }

    public void setDistanceToNextStation(float distanceToNextStation) {
        this.distanceToNextStation = distanceToNextStation;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public void setDistanceToPreStation(float distanceToPreStation) {
        this.distanceToPreStation = distanceToPreStation;
    }

    public void setTerminal(boolean terminal) {
        isTerminal = terminal;
    }

    public float getDistanceToNextStation() {
        return distanceToNextStation;
    }

    public float getDistanceToPreStation() {
        return distanceToPreStation;
    }

    public String getStationName() {
        return stationName;
    }

    public boolean isTerminal() {
        return isTerminal;
    }



}
