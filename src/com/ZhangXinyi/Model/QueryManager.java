package com.ZhangXinyi.Model;

import com.ZhangXinyi.DA.QueryManagerDA;
import java.util.*;

public class QueryManager {

    private List<MetroLine> metroLineList;
    private Map<Station,Integer> stationMap;
    private int[][] adjMatrix;
    private float[][] weightMap;
    private int totalStation;
    private Set<String> transferStationSet;

    private final int BASE_FARE = 2;
    private static final int BASE_DISTANCE = 9;
    public static final int ONE_WAY_TICKET = 0;
    public static final int TRAFFIC_CARD = 1;
    public static final int DAYS_PASS = 2;

    public QueryManager(List<MetroLine> metroLines){
        this.metroLineList = metroLines;
        transferStationSet = new HashSet<>();
        calculateTransferStation();

        //计算总站数
        int i =0;
        for (MetroLine line:metroLineList){
            for (Station station:line.getLineStationList()){
                if(transferStationSet.contains(station.getStationName())){
                    continue;
                }else {
                    i++;
                }
            }
        }
        i+=transferStationSet.size();
        totalStation = i;
        calculateAdjMatrix();
        calculateWeightMap();

        double fare = queryFare("光谷广场","天河机场",TRAFFIC_CARD);

    }

    /**
     * 计算站点的邻接矩阵
     */
    private void calculateAdjMatrix(){
        adjMatrix = new int[totalStation][totalStation];
        List<Station> totalStationList = new LinkedList<>();
        stationMap = new LinkedHashMap<>();

        for (MetroLine line:metroLineList){
            totalStationList.addAll(line.getLineStationList());
        }

        //构建stationMap Key为Station对象，Value为邻接矩阵对应的序列号
        int temp =0;
        for (Station station:totalStationList){
            if(transferStationSet.contains(station.getStationName()))
            {
                boolean isContain = false;
                for (Map.Entry<Station,Integer> entry:stationMap.entrySet()){
                    if(entry.getKey().getStationName().equals(station.getStationName())){
                        stationMap.put(station,entry.getValue());
                        isContain = true;
                        break;
                    }
                }
                if(!isContain){
                    stationMap.put(station,temp);
                    temp++;
                }
            }else {
                stationMap.put(station,temp);
                temp ++;
            }
        }

        //计算邻接矩阵
        for (int i=0;i<totalStation;i++){
            List<Station> tempList = this.getStations(i);
            for (Station station:tempList){
                int index = totalStationList.indexOf(station);
                if (station.isTerminal()){
                    if(station.getDistanceToNextStation() == 0){
                        adjMatrix[i][stationMap.get(totalStationList.get(index-1))] = 1;
                    }else {
                        if(index<totalStationList.size()-1){
                            adjMatrix[i][stationMap.get(totalStationList.get(index+1))] = 1;
                        }
                    }
                }else {
                    adjMatrix[i][stationMap.get(totalStationList.get(index-1))] = 1;
                    adjMatrix[i][stationMap.get(totalStationList.get(index+1))] = 1;
                }
            }
        }

        //打印邻接矩阵
        /*
        for (int i =0; i<totalStation;i++){
            for (int q=0;q<totalStation;q++){
                System.out.print(adjMatrix[i][q] + "  ");
            }
            System.out.println();
        }*/
    }

    /**
     * 计算各点间的权重图
     */
    private void calculateWeightMap(){
        weightMap = new float[totalStation][totalStation];
        for (int i = 0;i<totalStation;i++){
            for (int q = 0;q<totalStation;q++){
                if(q==i)
                {
                    weightMap[i][q] = 0;
                }
                else {
                    if(adjMatrix[i][q] == 0){
                        weightMap[i][q] = Float.POSITIVE_INFINITY;
                    }else {
                        List<Station> startList = getStations(i);
                        List<Station> destList = getStations(q);
                        for (Station start:startList){
                            for (Station dest:destList){
                                for (MetroLine line:metroLineList){
                                    if(line.isAdjacentStation(start,dest)){
                                        if(start.getDistanceToNextStation()==dest.getDistanceToPreStation()){
                                            weightMap[i][q] = start.getDistanceToNextStation();
                                        }else {
                                            weightMap[i][q] = start.getDistanceToPreStation();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 获得对应序列的Station List
     * @param indexOfMap 邻接矩阵序列
     * @return 对应序列站点集
     * @throws IndexOutOfBoundsException indexOfMap超出矩阵范围
     */
    private List<Station> getStations(int indexOfMap) throws IndexOutOfBoundsException{
        if(indexOfMap>=stationMap.size()||indexOfMap<0){
            throw new IndexOutOfBoundsException("Station Index Don Not Belonged to Matrix");
        }else {
            LinkedList<Station> stationLinkedList =new LinkedList<>();
            for (Map.Entry<Station,Integer> entry:stationMap.entrySet()){
                if(entry.getValue()==indexOfMap){
                    stationLinkedList.add(entry.getKey());
                }
            }
            return stationLinkedList;
        }
    }

    /**
     * 返回对应站点名在邻接矩阵中的序列
     * @param stationName 站点名
     * @return 其位于邻接矩阵的序列，-1为无法在图中找到
     */
    private int getStationIndexOfMap(String stationName){
        for(MetroLine line:metroLineList){
            for (Station station:line.getLineStationList()){
                if(station.getStationName().equals(stationName)){
                    return stationMap.get(station);
                }
            }
        }
        return -1;
    }

    /**
     * 获取全部换乘车站站名
     */
    private void calculateTransferStation(){
        for (MetroLine line:metroLineList){
            for (Station station:line.getLineStationList()){
                for (MetroLine temp:metroLineList){
                    if(temp.equals(line)){
                        continue;
                    }
                    else if (temp.isContainStation(station)) {
                        transferStationSet.add(station.getStationName());
                    }
                }
            }
        }
    }

    /**
     * 根据dijkstra算法获取最短路径
     * @author Zhang Xinyi
     * @param startStationName 始发站
     * @param destinationStationName 终点站
     * @return Map，Key：String（Index，Distance），Value：List包含矩阵序列与距离
     */
    private Map<String,List> dijkstraAlgo(String startStationName,String destinationStationName){
        int startIndex = getStationIndexOfMap(startStationName);
        int endIndex = getStationIndexOfMap(destinationStationName);
        if(startIndex<0||endIndex<0){
            return null;
        }else {
            int[] previous = new int[totalStation];

            for (int i = 0;i<previous.length;i++){
                previous[i] = -1;
            }

            Set<Integer> definedSet = new HashSet<>();
            Set<Integer> undefinedSet = new HashSet<>();
            float[] distance = new float[totalStation];

            System.arraycopy(weightMap[startIndex], 0, distance, 0, totalStation);

            definedSet.add(startIndex);
            for (int i = 0;i<totalStation;i++){
                if(i!=startIndex){
                    undefinedSet.add(i);
                }
            }

            //算法实质
            while (!undefinedSet.isEmpty()){
                float min = Float.POSITIVE_INFINITY;
                int minIndex = -1;
                for (Integer i:undefinedSet){
                    if(distance[i]<min){
                        min = distance[i];
                        minIndex = i;
                    }
                }
                if(minIndex == endIndex)
                {
                    break;
                }
                else if(minIndex>=0){
                    definedSet.add(minIndex);
                    undefinedSet.remove(minIndex);
                    float[] pointToOtherPointsDistance = weightMap[minIndex];
                    float[] newDisTemp = new float[totalStation];
                    for (int i :undefinedSet){
                        newDisTemp[i] = distance[minIndex]+pointToOtherPointsDistance[i];
                        if(newDisTemp[i]<distance[i]){
                            distance[i] = newDisTemp[i];
                            previous[i] = minIndex;
                        }
                    }
                }else {
                    return null;
                }
            }

            Map<String,List> targetMap = new HashMap<>();
            List<Float> distanceList = new ArrayList<>();
            distanceList.add(distance[endIndex]);

            //追溯获得线路沿途站点
            List<Integer> stationIndexList = new ArrayList<>();
            int temp=endIndex;
            while (temp!=-1){
                stationIndexList.add(temp);
                temp = previous[temp];
            }
            stationIndexList.add(startIndex);

            List<Integer>  stationList = new LinkedList<>();
            for (int i=stationIndexList.size()-1;i>=0;i--){
                stationList.add(stationIndexList.get(i));
            }

            targetMap.put("Index",stationList);
            targetMap.put("Distance",distanceList);

            return targetMap;
        }
    }

    /**
     * 获取两站间沿途站点线路图
     * @param startName 起始站站名
     * @param destinationName 终点站站名
     * @return Key：对应MetroLine对象 Value:线路途经的站点数.换乘站为每个Key之前所有Key对应的Integer之和的序列
     */
    private LinkedHashMap<MetroLine,Integer> getRouteLineMap(String startName,String destinationName){
        String[] stationNameArr = queryStationsByShortestWay(startName,destinationName);
        if(stationNameArr==null||stationNameArr.length<2)
        {
            return null;
        }else {
            LinkedHashMap<MetroLine,Integer> routeLineMap  = new LinkedHashMap<>();
            for (MetroLine line:metroLineList){
                if(line.isAdjacentStation(stationNameArr[0],stationNameArr[1])){
                    routeLineMap.put(line,2);
                    break;
                }
            }

            for (int i = 2;i<stationNameArr.length;i++){
                MetroLine lastLine;
                Iterator<Map.Entry<MetroLine, Integer>> iterator = routeLineMap.entrySet().iterator();
                Map.Entry<MetroLine, Integer> tail = null;
                while (iterator.hasNext()) {
                    tail = iterator.next();
                }
                lastLine = tail.getKey();
                for (MetroLine line:metroLineList){
                    if(line.isAdjacentStation(stationNameArr[i-1],stationNameArr[i])){
                        if(!lastLine.equals(line)){
                            MetroLine line_temp = new MetroLine(null,line.getLineName());
                            if(lastLine.getLineName().equals(line_temp.getLineName()))
                            {
                                routeLineMap.put(lastLine,tail.getValue()+1);
                                break;
                            }else {
                                if(routeLineMap.containsKey(line))
                                {
                                    routeLineMap.put(line_temp,1);
                                }else
                                {
                                    routeLineMap.put(line,1);
                                }
                                break;
                            }
                        }
                        else {
                            routeLineMap.put(line,tail.getValue()+1);
                            break;
                        }
                    }
                }
            }
            return routeLineMap;
        }
    }

    /**
     * 计算基础票价
     * @param distance 乘车距离
     * @return 基础票价
     */
    private int calculateBaseFare (float distance){
        int n = 0;
        int distanceRange = BASE_DISTANCE;
        while (distance>(distanceRange+5*n+n*(n+1))){
            n++;
        }
        return BASE_FARE+n;
    }

    /**
     * 初始化数据库访问
     * @param dbPath 数据库路径
     */
    public static void initialize(String dbPath){
        QueryManagerDA.initialize(dbPath);
    }

    /**
     * 关闭数据库
     */
    public static void terminate(){
        QueryManagerDA.terminate();
    }

    /**
     * 获取包含全部线路的QueryManager对象
     * @return 包含全部线路信息的QueryManager对象
     */
    public static QueryManager getAllLineFromDB(){
        return QueryManagerDA.getAllLineFromDB();
    }

    /**
     * 功能1接口，返回对应站点的线路名
     * @param stationName 站点名
     * @return 线路名字符串数组,返回null表示无该站点
     */
    public String[] queryLineByStationName (String stationName){
        int index = getStationIndexOfMap(stationName);
        try {
            List<Station> stationList = getStations(index);
            List<String> lineNameList = new LinkedList<>();
            for (Station station:stationList){
                for (MetroLine metroLine:metroLineList){
                    if(metroLine.getLineStationList().contains(station)){
                        lineNameList.add(metroLine.getLineName());
                    }
                }
            }
            String[] lineNameArr = new String[lineNameList.size()];
            return lineNameList.toArray(lineNameArr);
        }catch (IndexOutOfBoundsException e){
            e.getMessage();
        }
        return null;

    }

    /**
     * 通过给定线路名和终点站名（方向），按方向输出该线路全部站点
     * @param lineName 线路名
     * @param terminalStationName 终点站名
     * @return 站点Station对象数组
     */
    public Station[] queryLineStationListByTerminalDir(String lineName,String terminalStationName){
        MetroLine tempLine = null;
        boolean isPositiveDir = true;
        for (MetroLine metroLine:metroLineList){
            if(metroLine.getLineName().contains(lineName)){
                tempLine = metroLine;
                if(tempLine.getLineStationList().get(0).getStationName().equals(terminalStationName)){
                    isPositiveDir = true;
                }else if(tempLine.getLineStationList().get(tempLine.getLineStationList().size()-1).getStationName().equals(terminalStationName)){
                    isPositiveDir = false;
                }else {
                    return null;
                }
                break;
            }
        }

        if (tempLine == null){
            return null;
        }else {
            if(!isPositiveDir){
                Station[] stationArr = new Station[tempLine.getLineStationList().size()];
                return tempLine.getLineStationList().toArray(stationArr);
            }else {
                List<Station> stationList = new ArrayList<>();
                for (int i = tempLine.getLineStationList().size()-1;i>=0;i--){
                    stationList.add(tempLine.getLineStationList().get(i));
                }
                Station[] stationArr = new Station[tempLine.getLineStationList().size()];
                return stationList.toArray(stationArr);
            }

        }
    }

    /**
     * 返回两站之间最短路径途经的站点名称，暴露给外面的查询借口
     * @param startName 起始站站名
     * @param destinationName 终点站站名
     * @return 返回站点名数组，null表示站点名称错误或两站不存在联通路
     */
    public String[] queryStationsByShortestWay(String startName,String destinationName){
        Map<String,List> map = dijkstraAlgo(startName,destinationName);
        if (map == null)
        {
            return null;
        }else
        {
            Integer[] indexArr = new Integer[map.get("Index").size()];
            map.get("Index").toArray(indexArr);
            List<String> stationNameList = new LinkedList<>();
            for (Integer i:indexArr){
                List<Station> temp = getStations(i);
                stationNameList.add(temp.get(0).getStationName());
            }
            return stationNameList.toArray(new String[stationNameList.size()]);
        }
    }

    /**
     * 获取路径简介介绍
     * @param startName 始发站名
     * @param destinationName 终点站名
     * @return 描述语句数组,null为站点名称错误
     */
    public String[] queryWayInConciseWay(String startName,String destinationName){
        LinkedHashMap<MetroLine,Integer> routeMap = getRouteLineMap(startName,destinationName);
        String[] stationNameArr = queryStationsByShortestWay(startName,destinationName);
        List<String> target = new LinkedList<>();
        int routeStations = 0;
        if(routeMap == null)
        {
            return null;
        }
        else {
            for (Map.Entry<MetroLine,Integer> entry:routeMap.entrySet()){
                if(routeStations == 0){
                    routeStations+=entry.getValue();
                    String temp = String.format("乘坐%s,从%s站到%s站\n", entry.getKey().getLineName(),stationNameArr[0],stationNameArr[routeStations-1]);
                    target.add(temp);
                }else {
                    String temp = String.format("在%s站换乘%s,",stationNameArr[routeStations-1],entry.getKey().getLineName());
                    routeStations+=entry.getValue();
                    temp+=("到"+stationNameArr[routeStations-1]+"站\n");
                    target.add(temp);
                }
            }
            String[] strs = new String[target.size()];
            return target.toArray(strs);
        }
    }

    /**
     * 根据起始站和终点站计算不同票种费用
     * @param startName 起始站站名
     * @param destinationName 终点站站名
     * @param ticketType 票种
     * @return 费用，如果返回-1为票种类型错误，-2为站点信息错误
     */
    public double queryFare(String startName,String destinationName,int ticketType){
        Map<String,List> map = dijkstraAlgo(startName,destinationName);
        if(map!=null){
            float distance =(float) map.get("Distance").get(0);
            switch (ticketType){
                case ONE_WAY_TICKET:
                    return calculateBaseFare(distance);
                case TRAFFIC_CARD:
                    return calculateBaseFare(distance)*0.9;
                case DAYS_PASS:
                    return 0;
                    default:
                        return -1;
            }
        }else {
            return -2;
        }
    }
}