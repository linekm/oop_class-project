package com.ZhangXinyi.Model;

import com.ZhangXinyi.DA.MetroLineDA;

import java.util.List;

public class MetroLine {
    private String lineName;
    private List<Station> lineStationList;

    public MetroLine(List<Station> stationList , String lineName){
        this.lineStationList = stationList;
        this.lineName = lineName;
    }

    public List<Station> getLineStationList() {
        return lineStationList;
    }

    public String getLineName() {
        return lineName;
    }

    /**
     * 将本线路添加到数据库
     */
    public void addNewLine(){
        MetroLineDA.addNewLine(this);
    }

    /**
     * 初始化地铁线路数据库链接
     * @param dbPath 数据库路径
     */
    public static void initLineDA(String dbPath){
        MetroLineDA.initialize(dbPath);
    }

    /**
     * 关闭数据库
     */
    public static void terminate(){
        MetroLineDA.terminate();
    }

    /**
     * 从数据库获取对应线路的MetroLine对象
     * @param lineName 线路名称 e.g:"1号线"
     * @return 对应线路MetroLine对象
     */
    public static MetroLine findLine(String lineName){
        return MetroLineDA.find(lineName);
    }

    /**
     * 站点是否在该线路内
     * @param station 要查询的站点
     * @return isContained? true:false
     */
    boolean isContainStation(Station station){
        String stationName = station.getStationName();
        for (Station temp:lineStationList){
            if(temp.getStationName().equals(stationName))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 站点是否在该线路内
     * @param stationName 要查询的站点名称
     * @return isContained? true:false
     */
    boolean isContainStation(String stationName){
        for (Station temp:lineStationList){
            if(temp.getStationName().equals(stationName))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断两站是否为该线路相邻两站
     * @param s1 站点对象
     * @param s2 站点对象
     * @return isAdjacentStation? true:false
     */
    boolean isAdjacentStation(Station s1, Station s2){
        return s1.getDistanceToNextStation() == s2.getDistanceToPreStation() || s1.getDistanceToPreStation() == s2.getDistanceToNextStation();
    }

    /**
     * 判断两站是否为该线路相邻两站
     * @param s1Name 站点名
     * @param s2Name 站点名
     * @return isAdjacentStation? true:false
     */
    boolean isAdjacentStation(String s1Name, String s2Name){
        Station s1=findStation(s1Name),s2=findStation(s2Name);
        if(s1==null||s2==null)
        {
            return false;
        }else
        {
            return isAdjacentStation(s1,s2);
        }
    }

    /**
     * 根据站点名获取Station对象
     * @param stationName 站点名
     * @return Station 对象
     */
    Station findStation(String stationName){
        for (Station station:lineStationList){
            if(station.getStationName().equals(stationName)){
                return station;
            }
        }
        return null;
    }


}
