/**
 * 本文件为InitData类文件，该类为通过文件初始化数据库，调用initDataBase()静态方法进行初始化
 */
package com.ZhangXinyi.DA;

import com.ZhangXinyi.Model.MetroLine;
import com.ZhangXinyi.Model.Station;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class InitDataBase {
    /**
     * 初始化数据库通过文件
     * @param sourceFilePath 地铁信息源文件路径
     */
    public static void initDataBase(String sourceFilePath,String dbPath){
        File file = new File(sourceFilePath);
        File db = new File(dbPath);
        if(!db.exists()){

            //文件读取操作
            List<MetroLine> lineList = new LinkedList<>();
            try {
                List<Station> stationList = new LinkedList<>();
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

                boolean isLineStart = false;
                String lineName = "";
                try {
                    String line = bufferedReader.readLine();
                    while (line != null )
                    {
                        if(isLineStart == false && line.contains("站点间距")){
                            lineName = line.split("站点间距")[0];
                            isLineStart = true;
                            //越过空格
                            bufferedReader.readLine();
                            bufferedReader.readLine();
                            bufferedReader.readLine();
                            line = bufferedReader.readLine();
                        }else {
                            line = bufferedReader.readLine();
                            continue;
                        }

                        if(isLineStart){
                            int i = 0;
                            while (line !=null)
                            {
                                String[] temp1=line.split("---");
                                String[] temp2=temp1[1].split("\\t");
                                if(i==0)
                                {
                                    Station station1 = new Station(temp1[0],true,Float.valueOf(temp2[1]),0);
                                    stationList.add(station1);
                                }else {
                                    Station station1 = new Station(temp1[0],false,Float.valueOf(temp2[1]));
                                    stationList.add(station1);
                                }
                                line = bufferedReader.readLine();
                                i++;
                                if(line == null || line.isEmpty() || line.contains(" ")){
                                    i = 0;
                                    isLineStart = false;
                                    Station station2 = new Station(temp2[0],true,0);
                                    stationList.add(station2);
                                    //补全站点前站距离
                                    for (int index = stationList.size()-1;index>0 ; index--){
                                        stationList.get(index).setDistanceToPreStation(stationList.get(index-1).getDistanceToNextStation());
                                    }

                                    lineList.add(new MetroLine(stationList,lineName));
                                    stationList = new LinkedList<>();

                                    bufferedReader.readLine();
                                    line = bufferedReader.readLine();
                                    break;
                                }
                            }
                        }
                    }
                    bufferedReader.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            //数据库操作
            Connection dbConnection = null;
            try {
                Class.forName("org.sqlite.JDBC");
                dbConnection = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("Opened database successfully");
                Statement statement = dbConnection.createStatement();
                for (MetroLine line:lineList) {
                    String sql = String.format
                            (
                                    "CREATE TABLE IF NOT EXISTS %s("+
                                            "Id INTEGER PRIMARY KEY        AUTOINCREMENT,"+
                                            "Station         TEXT       NOT NULL,"+
                                            "PreDistance     REAL      NOT NULL,"+
                                            "NextDistance    REAL        NOT NULL,"+
                                            "isTerminal      TEXT        NOT NULL);"
                                    ,"地铁"+line.getLineName()
                            );
                    System.out.println(sql);
                    statement.executeUpdate(sql);
                    MetroLineDA.addStation(line, statement);
                }

                statement.close();
                dbConnection.close();

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
