package com.ZhangXinyi.DA;

import com.ZhangXinyi.Model.MetroLine;
import com.ZhangXinyi.Model.QueryManager;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class QueryManagerDA {

    private static QueryManager aQueryManager;
    private static Connection aConnection;
    private static Statement aStatement;

    private static List<MetroLine> aMetroLineList;

    /**
     * 创建连接数据库的Connection 与 Statement，如果数据库不存在，则根据source/subway.txt于给定路径来创建数据库
     * @param dbPath 数据库路径
     */
    public static void initialize(String dbPath){
        try {
            File file = new File(dbPath);
            if(!file.exists()) {
                InitDataBase.initDataBase("source/subway.txt",dbPath);
            }
            aConnection = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
            System.out.println("Open Database Successfully");
            aStatement = aConnection.createStatement();
            MetroLineDA.initialize(dbPath);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 关闭数据库
     */
    public static void terminate(){
        try {
            aStatement.close();
            aConnection.close();
            MetroLineDA.terminate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取一个QueryManager对象，包含各个线路信息
     */
    public static QueryManager getAllLineFromDB(){
        String sql = "SELECT name FROM sqlite_master WHERE type='table' ORDER BY name; ";
        try {
            ArrayList<String> lineName = new ArrayList<>();
            ResultSet rs = aStatement.executeQuery(sql);
            aMetroLineList = new LinkedList<>();
            int i = 0;
            while (rs.next()){
                if (i == 0){
                    i++;
                    continue;
                }
                //System.out.println(rs.getString("name"));
                lineName.add(rs.getString("name"));
                i++;
            }
            for (String temp:lineName){
                aMetroLineList.add(MetroLineDA.find(temp));
            }
            aQueryManager = new QueryManager(aMetroLineList);
            return aQueryManager;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
