package com.ZhangXinyi.DA;

import com.ZhangXinyi.Model.MetroLine;
import com.ZhangXinyi.Model.Station;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class MetroLineDA {
    private static MetroLine aMetroLine;
    private static Connection aConnection;
    private static Statement aStatement;

    private static String aLineName;
    private static List<Station> aStationList;

    /**
     * 创建连接数据库的Connection 与 Statement
     * @param dbPath 数据库路径
     */
    public static void initialize(String dbPath){
        try {
            aConnection = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
            System.out.println("Open Database Successfully");
            aStatement = aConnection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 关闭数据库
     */
    public static void terminate(){
        try {
            aStatement.close();
            aConnection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 从数据库中查找对应线路
     * @param lineName 线路名称 e.g:"1号线"
     * @return 包含完整站点信息的MetroLine对象
     */
    public static MetroLine find(String lineName){
        aMetroLine = null;
        aStationList = new LinkedList<>();
        String sql = String.format("SELECT * FROM %s",lineName);
        try {
            ResultSet resultSet = aStatement.executeQuery(sql);
            while (resultSet.next()){
                String stationName = resultSet.getString("Station");
                float distanceToPre = resultSet.getFloat("PreDistance");
                float distanceToNext = resultSet.getFloat("NextDistance");
                boolean isTerminal = Boolean.valueOf(resultSet.getString("isTerminal"));
                Station station = new Station(stationName,isTerminal,distanceToNext,distanceToPre);
                aStationList.add(station);
            }
            aMetroLine = new MetroLine(aStationList,lineName);
            return aMetroLine;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 添加一条新的地铁线路
     * @param metroLine 被添加对象
     */
    public static void addNewLine(MetroLine metroLine){
        try {
            Integer i = createLineTable(metroLine);
            addStation(metroLine, aStatement);
        }  catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static Integer createLineTable(MetroLine metroLine) throws SQLException {
        String sql = String.format
                (
                        "CREATE TABLE IF NOT EXISTS %s("+
                                "Id INT PRIMARY KEY        NOT NULL,"+
                                "Station         TEXT       NOT NULL,"+
                                "PreDistance     REAL      NOT NULL,"+
                                "NextDistance    REAL        NOT NULL,"+
                                "isTerminal      TEXT        NOT NULL);"
                        ,"地铁"+metroLine.getLineName()
                );
        System.out.println(sql);
        aStatement.executeUpdate(sql);
        return 0;
    }

    static void addStation(MetroLine metroLine, Statement aStatement) throws SQLException {
        String sql;
        for (Station station:metroLine.getLineStationList()) {
            sql = String.format
                    (
                            "INSERT INTO %s (Station,PreDistance,NextDistance,isTerminal)"
                                    + "VALUES('%s',%s,%s,'%s');",
                            "地铁" + metroLine.getLineName(),
                            String.valueOf(station.getStationName()),
                            String.valueOf(station.getDistanceToPreStation()),
                            String.valueOf(station.getDistanceToNextStation()),
                            String.valueOf(station.isTerminal())
                    );
            System.out.println(sql);
            aStatement.executeUpdate(sql);
        }
    }
}
